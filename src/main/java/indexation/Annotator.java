package indexation;

import edu.gatech.gtri.bktree.BkTreeSearcher;
import edu.gatech.gtri.bktree.MutableBkTree;
import edu.stanford.nlp.util.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Annotator {

    private final TextMiner textMiner;
    private BkTreeSearcher<String> bkTreeSearcher;

    public Annotator(List<String> vocabulary) {
        textMiner = new TextMiner();
        MutableBkTree<String> bkTree = new MutableBkTree<>((x, y) -> LevenshteinDistance.getDefaultInstance().apply(x, y));
        bkTree.addAll(vocabulary);
        bkTreeSearcher = new BkTreeSearcher<>(bkTree);
    }

    public List<String> annotate(String text, int maxNumberOfWords, int maxEditDistance) {
        if (StringUtils.isNullOrEmpty(text))
            return Collections.emptyList();

        return textMiner.getNominalGroups(text, maxNumberOfWords)
                .parallelStream()
                .flatMap(candidate -> search(candidate, maxEditDistance))
                .collect(Collectors.toList());
    }

    public Stream<String> search(String query, int maxEditDistance) {
        return bkTreeSearcher.search(query, maxEditDistance)
                .parallelStream()
                .map(BkTreeSearcher.Match::getMatch);
    }

}
