package indexation;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class TextMiner {

    private final StanfordCoreNLP pipeline;

    public TextMiner() {
        Properties pipelineProps = new Properties();
        pipelineProps.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse");
        pipelineProps.setProperty("threads", String.valueOf(Runtime.getRuntime().availableProcessors()));
        pipeline = new StanfordCoreNLP(pipelineProps);
    }

    public List<String> getNominalGroups(String text, int maxNumberOfWords) {
        long start = System.currentTimeMillis();

        Annotation annotation = new Annotation(text.toLowerCase());
        pipeline.annotate(annotation);
        Tree tree = annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0)
                .get(TreeCoreAnnotations.TreeAnnotation.class);

        List<String> nominalGroups = tree.subTreeList().stream()
                .filter(subTree -> subTree.label().toString().equals("NN") || subTree.label().toString().equals("NP"))
                .map(Tree::yieldWords)
                .filter(words -> words.size() <= maxNumberOfWords)
                .map(words -> words
                        .parallelStream()
                        .map(Word::word)
                        .collect(Collectors.joining(" ")))
                .distinct()
                .collect(Collectors.toList());

        System.out.println("NLP computation ended after " + (System.currentTimeMillis()-start) + "ms");
        return nominalGroups;
    }

    public static void main(String[] args) {
        TextMiner textMiner = new TextMiner();
        textMiner.getNominalGroups("Direct and indirect control of hepatic glucose production by insulin", 4)
                .forEach(System.out::println);
    }

}
