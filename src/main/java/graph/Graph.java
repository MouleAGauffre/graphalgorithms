package graph;

import lombok.Getter;

import java.util.ArrayList;

@Getter
public class Graph {

    private ArrayList<Node> nodes;

    public Graph() {
        nodes = new ArrayList<>();
    }

}
