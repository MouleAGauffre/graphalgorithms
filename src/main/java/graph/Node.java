package graph;

import lombok.Getter;

import java.util.ArrayList;

@Getter
public class Node {

    private long id;
    private ArrayList<Node> neighbours;

    public Node(long id) {
        this.id = id;
        neighbours = new ArrayList<>();
    }

    public void addNeighbour(Node neighbour) {
        if (!neighbours.contains(neighbour)) {
            neighbours.add(neighbour);
            neighbour.addNeighbour(this);
        }
    }

    @Override
    public String toString() {
        return "N" + id;
    }

}
