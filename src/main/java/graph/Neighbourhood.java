package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Neighbourhood {

    private List<Aggregator> aggregators;

    public Neighbourhood() {
        aggregators = new ArrayList<>();
    }

    public Map<Node, Float> compute(Node n0, float maxDistance) {
        System.out.println("Neighbourhood::compute(" + n0 + ", " + maxDistance + ")");
        long start = System.currentTimeMillis();

        Map<Node, Float> results = new HashMap<>();
        aggregators.add(new Aggregator(null, n0, Map.of(n0, 0f), 0f));

        while (!aggregators.isEmpty()) {
            Map<Boolean, List<Aggregator>> newAggregators = aggregators.parallelStream()
                    .flatMap(aggregator -> processMessage(aggregator, maxDistance).parallelStream())
                    .collect(Collectors.partitioningBy(aggregator -> aggregator.willPropagate(maxDistance)));

            aggregators = newAggregators.get(true);
            fetchResultsFromCompletedAggregators(newAggregators.get(false), results);
        }

        System.out.println("Neighbourhood computation ended after " + (System.currentTimeMillis()-start) + "ms");
        return results;
    }

    private void fetchResultsFromCompletedAggregators(List<Aggregator> completedAggregators, Map<Node, Float> results) {
        completedAggregators.parallelStream()
                .map(Aggregator::getPayload)
                .flatMap(aggregatorPayload -> aggregatorPayload.entrySet().parallelStream())
                .collect(Collectors.groupingBy(Map.Entry::getKey))
                .forEach((node, distances) -> {
                    float minDistance = distances.parallelStream()
                            .map(Map.Entry::getValue)
                            .min(Comparator.naturalOrder())
                            .orElse(0f);
                    if (!results.containsKey(node) || (results.containsKey(node) && minDistance < results.get(node))) {
                        results.put(node, minDistance);
                    }
                });
    }

    private List<Aggregator> processMessage(Aggregator msg, float maxDistance) {
        if (msg.willPropagate(maxDistance)) {
            return msg.getDestination().getNeighbours().parallelStream()
                    .map(node -> Aggregator.create(msg, 1f, node))
                    .collect(Collectors.toList());

        } else {
            return Collections.emptyList();
        }
    }

}
