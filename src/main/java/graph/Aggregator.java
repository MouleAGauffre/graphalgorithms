package graph;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public class Aggregator {

    private Node sender;
    private Node destination;
    private Map<Node, Float> payload;
    private float distance;

    public static Aggregator create(Aggregator previousMsg, float edgeLength, Node destination) {
        Map<Node, Float> newPayload = new HashMap<>(previousMsg.getPayload());
        float newDistance = previousMsg.getDistance()+edgeLength;
        newPayload.put(destination, previousMsg.getDistance()+edgeLength);
        return new Aggregator(previousMsg.getDestination(), destination, newPayload, newDistance);
    }

    public boolean willPropagate(float maxDistance) {
        return distance < maxDistance || (payload.containsKey(destination) && distance < payload.get(destination));
    }

    @Override
    public String toString() {
        return "Aggregator(" +
                sender +
                "->" + destination +
                ", payload=" + payload +
                ", distance=" + distance +
                ')';
    }
}
