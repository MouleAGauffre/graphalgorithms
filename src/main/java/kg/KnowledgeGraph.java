package kg;

import graph.Graph;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class KnowledgeGraph extends Graph {

    public void loadOntology(String rdfFilePath) {
        InputStream in = RDFDataMgr.open(rdfFilePath);

        if (in == null) {
            throw new IllegalArgumentException(rdfFilePath + " not found");
        }

        Model model = ModelFactory.createDefaultModel();
        model.read(in, null);

        Set<Descriptor> classesSet = new HashSet<>();

        /*model.listStatements().toList()
                .parallelStream()
                .filter(stmt -> stmt.getPredicate().getURI().equals(RDFS.subClassOf.getURI()))
                .filter(stmt -> stmt.getSubject().hasProperty(RDFS.label)
                        && stmt.getObject().asResource().hasProperty(RDFS.label))
                .forEachOrdered(stmt ->
                        System.out.println(stmt.getSubject().getProperty(RDFS.label).getString()
                                + "->"
                                + stmt.getObject().asResource().getProperty(RDFS.label).getString()));*/

    }

}
