package kg;

import graph.Node;

import java.util.List;

public class Descriptor extends Node {

    private String mainLabel;
    private List<String> altLabels;

    public Descriptor(long id, String label) {
        super(id);
        mainLabel = label;
    }

    public Descriptor(long id, String mainLabel, List<String> altLabels) {
        super(id);
        this.mainLabel = mainLabel;
        this.altLabels = altLabels;
    }

}
