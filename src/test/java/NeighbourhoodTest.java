import graph.Neighbourhood;
import graph.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class NeighbourhoodTest {

    private Node n1;
    private Node n2;
    private Node n3;
    private Node n4;
    private Node n5;
    private Node n6;

    @Before
    public void setup() {
        n1 = new Node(1);
        n2 = new Node(2);
        n3 = new Node(3);
        n4 = new Node(4);
        n5 = new Node(5);
        n6 = new Node(6);

        n1.addNeighbour(n2);
        n1.addNeighbour(n3);

        n2.addNeighbour(n3);

        n3.addNeighbour(n4);
        n3.addNeighbour(n5);

        n5.addNeighbour(n6);
    }

    @Test
    public void shouldComputeNeighbourhoodCorrectly() {
        Neighbourhood neighbourhood = new Neighbourhood();
        Map<Node, Float> result = neighbourhood.compute(n1, 3f);

        System.out.println("\nResults:");
        result.forEach((node, distance) -> System.out.println("n" + node.getId() + ": " + distance));

        Assert.assertTrue("d(N1)=0", result.containsKey(n1) && result.get(n1) == 0f);
        Assert.assertTrue("d(N2)=1", result.containsKey(n2) && result.get(n2) == 1f);
        Assert.assertTrue("d(N3)=1", result.containsKey(n3) && result.get(n3) == 1f);
        Assert.assertTrue("d(N4)=2", result.containsKey(n4) && result.get(n4) == 2f);
        Assert.assertTrue("d(N5)=2", result.containsKey(n5) && result.get(n5) == 2f);
        Assert.assertTrue("d(N6)=3", result.containsKey(n6) && result.get(n6) == 3f);
        Assert.assertEquals("Correct number of results", result.size(), 6);
    }

}
