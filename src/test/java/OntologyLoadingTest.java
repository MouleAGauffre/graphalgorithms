import kg.KnowledgeGraph;
import org.junit.Test;

public class OntologyLoadingTest {

    private KnowledgeGraph kg;

    @Test
    public void shouldLoadRDF() {
        kg = new KnowledgeGraph();
        kg.loadOntology("src/test/resources/codo.xrdf");
    }

}
