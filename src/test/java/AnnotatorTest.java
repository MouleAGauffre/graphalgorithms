import indexation.Annotator;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class AnnotatorTest {

    private Annotator annotator;

    @Before
    public void setup() {
        annotator = new Annotator(List.of("hepatic glucose production", "glucose", "insulin", "hypnotic glucose", "insuline"));
    }

    @Test
    public void shouldAnnotateCorrectly() {
        String text = "Direct and indirect control of hepatic glucose production by insulin";
        List<String> annotations = annotator.annotate(text, 4, 3);

        MatcherAssert.assertThat("Correct annotations", annotations, CoreMatchers.hasItems(
                "hepatic glucose production",
                "glucose",
                "insulin",
                "insuline"));
        Assert.assertEquals("Correct number of annotations", annotations.size(), 4);
    }

}
